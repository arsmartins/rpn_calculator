/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatorrpn;

/**
 *
 * @author amartins
 */
public class ComplexNumber {
    private double a, b;
    private final double precision = 0.0005d;
    
    /**
     *  Class constructor in rectangular coordinates
     * 
     * @param u real component
     * @param v imaginary component
     */
    public ComplexNumber(double u, double v) {
        this.a = u;
        this.b = v;
    }
    
    /**
     * Returns the sum of two complex numbers
     * 
     * @param z complex number to add
     * @return  complex number representing the sum
     */
    public ComplexNumber add(ComplexNumber z) {
        ComplexNumber result = new ComplexNumber(Double.NaN, Double.NaN);
        result.a = this.a + z.a;
        result.b = this.b + z.b;
        return result;
    }
    
    /**
     * Returns the subtraction of two complex numbers
     * 
     * @param z complex number to subtract
     * @return  complex number representing the subtraction
     */
     public ComplexNumber subtract(ComplexNumber z) {
        ComplexNumber result = new ComplexNumber(Double.NaN, Double.NaN);
        result.a = this.a - z.a;
        result.b = this.b - z.b;
        return result;
    }
     
     /**
     * Returns the multiplication of two complex numbers
     * 
     * @param z complex number to multiply
     * @return  complex number representing the multiplication
     */
     public ComplexNumber multiply(ComplexNumber z) {
        ComplexNumber result = new ComplexNumber(Double.NaN, Double.NaN);
        result.a = this.a * z.a - this.b * z.b;
        result.b = this.b * z.a + this.a * z.b;
        return result;
    }
     
     /**
     * Returns the division of two complex numbers
     * 
     * @param z complex number to divide (denominator)
     * @return  complex number representing the division
     */
     public ComplexNumber divide(ComplexNumber z) {
        ComplexNumber result = new ComplexNumber(Double.NaN, Double.NaN);
        result.a = (this.a * z.a + this.b * z.b)/(z.a*z.a + z.b*z.b);
        result.b = (this.b * z.a - this.a * z.b)/(z.a*z.a + z.b*z.b);
        return result;
    }
    
    @Override
    public String toString() {
        if(this.b>=0)
            return "(" + Double.toString(this.a) + " + " + Double.toString(this.b) + "j)";
        else
            return "(" + Double.toString(this.a) + " - " + Double.toString(Math.abs(this.b)) + "j)";
    }
    
    @Override
    public boolean equals(Object obj){
        boolean result = false;
        if(obj == this)
            return true;
        else
            if (obj instanceof ComplexNumber) {
                ComplexNumber aux = (ComplexNumber) obj;
                if (Double.isNaN(this.a) && Double.isNaN(aux.a)
                        && Double.isNaN(this.b) && Double.isNaN(aux.b)) {
                    result = true;
                } else {
                    result = (Math.abs(this.a - aux.a) < this.precision
                        && Math.abs(this.b - aux.b) < this.precision);
                }
            }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.precision) ^ (Double.doubleToLongBits(this.precision) >>> 32));
        return hash;
    }
}