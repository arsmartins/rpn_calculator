/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn;

/**
 *
 * @author amartins
 */
public class Calculator {

    ComplexNumberStack stack;

    public Calculator() {
        stack = new ComplexNumberStack();
    }

    /**
     * 
     */
    public void add() {
        ComplexNumber z1, z2;
        z1 = stack.pop();
        z2 = stack.pop();
        this.stack.push(z1.add(z2));
    }

    /**
     * 
     */
    public void subtract() {
        ComplexNumber z1, z2;
        z1 = stack.pop();
        z2 = stack.pop();
        this.stack.push(z1.subtract(z2));
    }

    /**
     * 
     */
    public void multiply() {
        ComplexNumber z1, z2;
        z1 = stack.pop();
        z2 = stack.pop();
        this.stack.push(z1.multiply(z2));
    }
    
    /**
     * 
     */
    public void divide() {
        ComplexNumber z1, z2;
        z1 = stack.pop();
        z2 = stack.pop();
        this.stack.push(z1.divide(z2));
    }
    
    /**
     * 
     * @return 
     */
    public String[] getStack() {
        String vec[];
        ComplexNumber vecNC[];

        vecNC = this.stack.getStackVec();
        if(vecNC!=null) {
            vec = new String[vecNC.length];
            int i=0;
            for(ComplexNumber z : vecNC)
                vec[i++] = z.toString();
            return vec;
        } else
            return null;
    }

    /**
     * 
     * @return 
     */
    public boolean drop() {
        ComplexNumber z1 = this.stack.pop();
        return !(z1 == null);
            
    }

    /**
     * 
     * @param st
     * @return 
     */
    public boolean addToStack(String st) {

        ComplexNumber z1 = parseNumber(st);
        if (z1 != null) {
            this.stack.push(z1);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return this.stack.toString();
    }

    /**
     * 
     * @param st
     * @return 
     */
    public ComplexNumber parseNumber(String st) {

        String aux = trimSpaces(st);
        if (aux.length() == 0) {
            return null;
        }
       
        int p = aux.indexOf("exp(");
        if (p > 0) {    // polar form
            try {
                double base = Double.valueOf(aux.substring(0, p - 1));
                double exponent = Double.valueOf(aux.substring(p + 4, aux.length() - 2)); // assumes the number ends in "j)" or "i)"
                return new ComplexNumber(base * Math.cos(exponent), base * Math.sin(exponent));
            } catch (NumberFormatException e) {
                return null;
            }
        } else {     // rectangular form
            aux = aux.replace("-", "#-");
            aux = aux.replace("+", "#+");
            String aux1[] = aux.split("#");
            
            if (aux1.length == 1) {   // real or imaginary only
                double img = 0d;
                double re = 0d;
                p = aux1[0].indexOf("j");
                try {
                    if (p >= 0) {
                        img = Double.valueOf(aux1[0].substring(0, p));
                    } else {
                        p = aux1[0].indexOf("i");
                        if (p >= 0) {
                            img = Double.valueOf(aux1[0].substring(0, p));
                        } else {
                            re = Double.valueOf(aux1[0]);
                        }
                    }
                    return new ComplexNumber(re, img);
                } catch (NumberFormatException e) {
                    return null;
                }
            } else if (aux1.length == 2) {     // real and imaginary components
                double img = 0d;
                double re = 0d;
                try {
                    re = Double.valueOf(aux1[0]);

                    p = aux1[1].indexOf("j");
                    if (p >= 0) {
                        img = Double.valueOf(aux1[1].substring(0, p));
                    } else {
                        p = aux1[1].indexOf("i");
                        if (p >= 0) {
                            img = Double.valueOf(aux1[1].substring(0, p));
                        } else {
                            return null;    // there is no imaginary component
                        }
                    }
                } catch (NumberFormatException e) {
                    return null;
                }
                return new ComplexNumber(re, img);
            } else // length > 2
            {
                return null;
            }
        }
    }

    /**
     * Eliminate spaces within the string
     *
     * @param st
     * @return
     */
    private String trimSpaces(String st) {
        String vecSt[] = st.split(" ");

        if (vecSt.length > 1) {
            String resSt = "";
            for (String aux : vecSt) 
                    resSt += aux;
            return resSt;
        } else {
            return st;
        }
    }
}
