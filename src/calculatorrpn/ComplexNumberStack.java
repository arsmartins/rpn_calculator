/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn;

import java.util.*;

/**
 *
 * @author amartins
 */
public class ComplexNumberStack {

    private ArrayDeque<ComplexNumber> stack;

    public ComplexNumberStack() {
        stack = new ArrayDeque<ComplexNumber>();
    }

    /**
     *
     * @return
     */
    public ComplexNumber pop() {
        try {
            return this.stack.pop();
        } catch (NoSuchElementException exception) {
            return null;
        }
    }

    /**
     *
     * @param z
     */
    public void push(ComplexNumber z) {
        if (z != null) {
            stack.push(z);
        }
    }

    /**
     *
     * @return
     */
    public ComplexNumber peek() {
        try {
            return this.stack.peek();
        } catch (NoSuchElementException exception) {
            return null;
        }
    }

    /**
     *
     * @return
     */
    public ComplexNumber[] getStackVec() {
        if (this.stack.size() > 0) {
            ComplexNumber vec[] = new ComplexNumber[this.stack.size()];
            int i = 0;

            for (ComplexNumber z : this.stack) {
                vec[i++] = z;
            }
            return vec;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return this.stack.toString();
    }

    @Override
    public boolean equals(Object obj) {
        Boolean result = false;
        if (this == obj) {
            return true;
        } else if (obj instanceof ComplexNumberStack) {
            ArrayDeque<ComplexNumber> aux = (ArrayDeque<ComplexNumber>) ((ComplexNumberStack) obj).stack;
            if (aux.size() == this.stack.size()) {
                result = true;
                Iterator<ComplexNumber> it2 = this.stack.iterator();
                for (Iterator<ComplexNumber> it1 = aux.iterator();
                        it1.hasNext() && it2.hasNext() && result;) {
                    ComplexNumber z1 = it1.next();
                    ComplexNumber z2 = it2.next();
                    result = z1.equals(z2);
                }
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.stack);
        return hash;
    }

}
