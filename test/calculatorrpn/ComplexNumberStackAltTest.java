/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatorrpn;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexNumberStackAltTest {
    ComplexNumberStack instance;
    
    public ComplexNumberStackAltTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 1d));
        instance.push(new ComplexNumber(1d, 2d));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of pop method, of class ComplexNumberStack.
     */
    @Test
    public void testPop() {
        System.out.println("pop");
        ComplexNumber expResult = new ComplexNumber(1d, 2d);
        ComplexNumber result = instance.pop();
        assertEquals(expResult, result);
    }

    /**
     * Test of push method, of class ComplexNumberStack.
     */
    @Test
    public void testPush() {
        System.out.println("push");
        ComplexNumber z = null;
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(z);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class ComplexNumberStack.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "[(1.0 + 2.0j), (1.0 + 1.0j)]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        ComplexNumberStack instance = new ComplexNumberStack();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hashCode method, of class ComplexNumberStack.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        ComplexNumberStack instance = new ComplexNumberStack();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
