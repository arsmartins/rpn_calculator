/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatorrpn;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexNumberTest {
    
    public ComplexNumberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ComplexNumber.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        ComplexNumber z = new ComplexNumber(1d, 2.1d);
        ComplexNumber instance = new ComplexNumber(-2d, 0.1d);
        ComplexNumber expResult = new ComplexNumber(-1d, 2.2d);
        ComplexNumber result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of subtract method, of class ComplexNumber.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        ComplexNumber z = new ComplexNumber(1d, 2.1d);
        ComplexNumber instance = new ComplexNumber(-2d, 0.1d);
        ComplexNumber expResult = new ComplexNumber(-3d, -2.0d);
        ComplexNumber result = instance.subtract(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of multiply method, of class ComplexNumber.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        ComplexNumber z = new ComplexNumber(1d, 2d);
        ComplexNumber instance = new ComplexNumber(2d, 2d);
        ComplexNumber expResult = new ComplexNumber(-2d, 6d);
        ComplexNumber result = instance.multiply(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of divide method, of class ComplexNumber.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        ComplexNumber z = new ComplexNumber(2d, 2d);
        ComplexNumber instance = new ComplexNumber(1d, 2d);
        ComplexNumber expResult = new ComplexNumber(0.75d, 0.25d);
        ComplexNumber result = instance.divide(z);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of divide method, of class ComplexNumber.
     */
    @Test
    public void testDivide1() {
        System.out.println("divide");
        ComplexNumber z = new ComplexNumber(2.015d, 1.234568d);
        ComplexNumber instance = new ComplexNumber(2.015d, 1.234567d);
        ComplexNumber expResult = new ComplexNumber(1d, 0d);
        ComplexNumber result = instance.divide(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of divide method, of class ComplexNumber.
     */
    @Test
    public void testDivideByZero() {
        System.out.println("divide by zero");
        ComplexNumber z = new ComplexNumber(0d, 0d);;
        ComplexNumber instance = new ComplexNumber(1d, 2d);
        ComplexNumber expResult = new ComplexNumber(Double.NaN, Double.NaN);
        ComplexNumber result = instance.divide(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ComplexNumber.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ComplexNumber instance = new ComplexNumber(1.25d, 3d);
        String expResult = "(1.25 + 3.0j)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class ComplexNumber.
     */
    @Test
    public void testToStringImgNeg() {
        System.out.println("toString with negative imaginary component");
        ComplexNumber instance = new ComplexNumber(1.25d, -3d);
        String expResult = "(1.25 - 3.0j)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new ComplexNumber(1.25d, 3d);
        ComplexNumber instance = new ComplexNumber(1.25005d, 3d);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("not equals");
        Object obj = new ComplexNumber(1.25d, 3d);
        ComplexNumber instance = new ComplexNumber(1.2506d, 3d);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEqualsNaN() {
        System.out.println("equals with NaN");
        Object obj = new ComplexNumber(Double.NaN, Double.NaN);
        ComplexNumber instance = new ComplexNumber(Double.NaN, Double.NaN);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
