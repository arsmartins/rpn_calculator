/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatorrpn;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class CalculatorTest {
    
    public CalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class Calculator.
     */
    @Test
    public void testAdd() {
        System.out.println("add - check stack result");
        Calculator instance = new Calculator();
        instance.addToStack("1 + 2j");
        instance.addToStack("1 - 2j");
        instance.add();
        
        ComplexNumberStack cns = new ComplexNumberStack();
        cns.push(new ComplexNumber(2d, 0d));
        
        assertEquals(cns, instance.stack);
    }

    /**
     * Test of subtract method, of class Calculator.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        Calculator instance = new Calculator();
        instance.subtract();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of multiply method, of class Calculator.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        Calculator instance = new Calculator();
        instance.multiply();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of divide method, of class Calculator.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        Calculator instance = new Calculator();
        instance.divide();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStack method, of class Calculator.
     */
    @Test
    public void testGetStack() {
        System.out.println("getStack");
        Calculator instance = new Calculator();
        instance.addToStack("2.3 + 6i");
        instance.addToStack("2 + 3j");
        instance.addToStack("2.52 - 3.0j");
        
        String[] expResult = {"(2.52 - 3.0j)", "(2.0 + 3.0j)", "(2.3 + 6.0j)"};
        String[] result = instance.getStack();
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of getStack method, of class Calculator.
     */
    @Test
    public void testGetStackEmpty() {
        System.out.println("getStack - empty stack");
        Calculator instance = new Calculator();        
        
        String[] result = instance.getStack();
        assertNull(result);
    }

    /**
     * Test of drop method, of class Calculator.
     */
    @Test
    public void testDrop() {
        System.out.println("drop");
        Calculator instance = new Calculator();
        instance.addToStack("2.3 + 6i");
        instance.addToStack("2 + 3j");
        instance.drop();
        String expResult = "[(2.3 + 6.0j)]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of drop method, of class Calculator.
     */
    @Test
    public void testDropEmptyStack() {
        System.out.println("drop from an empty stack");
        Calculator instance = new Calculator();
        assertTrue(!instance.drop());
    }

    /**
     * Test of addNumberToSTack method, of class Calculator.
     */
    @Test
    public void testAddNumberToSTack() {
        System.out.println("addNumberToSTack - wrong format");
        Calculator instance = new Calculator();
        instance.addToStack("2.3 + 6i");
        instance.addToStack("2j + 3");
        String expResult = "[(2.3 + 6.0j)]";
        assertEquals(expResult, instance.toString());
    }

    /**
     * Test of toString method, of class Calculator.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Calculator instance = new Calculator();
        instance.addToStack("2.3 + 6i");
        instance.addToStack("2 + 3j");
        String expResult = "[(2.0 + 3.0j), (2.3 + 6.0j)]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumber() {
        System.out.println("parseNumber - rectangular form, using i, ok");
        String st = " 1.0 + 2.31i";
        Calculator instance = new Calculator();
        ComplexNumber expResult = new ComplexNumber(1d, 2.31d);
        ComplexNumber result = instance.parseNumber(st);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumberNegImg() {
        System.out.println("parseNumber - rectangular form, using j, negative imaginary, ok");
        String st = " 1.0 - 2.31j";
        Calculator instance = new Calculator();
        ComplexNumber expResult = new ComplexNumber(1d, -2.31d);
        ComplexNumber result = instance.parseNumber(st);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumberRNotOK1() {
        System.out.println("parseNumber - rectangular form, using j, inverting order");
        String st = " 1.0j + 2.31";
        Calculator instance = new Calculator();

        ComplexNumber result = instance.parseNumber(st);
        assertNull(result);
    }
    
    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumberRNotOK2() {
        System.out.println("parseNumber - rectangular form, two reals");
        String st = " 1.0 + 2.31";
        Calculator instance = new Calculator();

        ComplexNumber result = instance.parseNumber(st);
        assertNull(result);
    }
    
    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumberRNotOK3() {
        System.out.println("parseNumber - rectangular form, two imaginary");
        String st = " 1.0j + 2.31j";
        Calculator instance = new Calculator();

        ComplexNumber result = instance.parseNumber(st);
        assertNull(result);
    }
    
    /**
     * Test of parseNumber method, of class Calculator.
     */
    @Test
    public void testParseNumberPOK() {
        System.out.println("parseNumber - polar coordinates");
        String st = "1.0 exp(-0.785398i)";
        Calculator instance = new Calculator();
        ComplexNumber expResult = new ComplexNumber(0.707107d, -0.707107d);
        ComplexNumber result = instance.parseNumber(st);
        assertEquals(expResult, result);
    }
}
