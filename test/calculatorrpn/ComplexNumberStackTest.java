/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatorrpn;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexNumberStackTest {
    
    public ComplexNumberStackTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of pop method, of class ComplexNumberStack.
     */
    @Test
    public void testPopEmptyStack() {
        System.out.println("pop with empty stack");
        ComplexNumberStack instance = new ComplexNumberStack();
        ComplexNumber result = instance.pop();
        assertNull( result);
    }
    
    /**
     * Test of pop method, of class ComplexNumberStack.
     */
    @Test
    public void testPop() {
        System.out.println("pop ");
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 1d));
        instance.push(new ComplexNumber(1d, 2d));
        ComplexNumber result = instance.pop();
        ComplexNumber expResult = new ComplexNumber(1d, 2d);
        assertEquals( result, expResult);
    }

    /**
     * Test of push method, of class ComplexNumberStack.
     */
    @Test
    public void testPush() {
        System.out.println("push null value");
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 1d));
        instance.push(new ComplexNumber(1d, 2d));
        ComplexNumber z = null;
        instance.push(z);
        
        ComplexNumberStack obj = new ComplexNumberStack();
        obj.push(new ComplexNumber(1d, 1d));
        obj.push(new ComplexNumber(1d, 2d));
        assertEquals(instance, obj);
    }

    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        ComplexNumberStack obj = new ComplexNumberStack();
        obj.push(new ComplexNumber(1d, 1d));
        obj.push(new ComplexNumber(1d, 2d));
        
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 1d));
        instance.push(new ComplexNumber(1d, 2d));
        
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("not equals");
        
        ComplexNumberStack obj = new ComplexNumberStack();
        obj.push(new ComplexNumber(1d, 2d));
        obj.push(new ComplexNumber(1d, 1d));
        
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 1d));
        instance.push(new ComplexNumber(1d, 2d));
        
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class ComplexNumberStack.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 0d));
        instance.push(new ComplexNumber(2d, 0d));
        instance.push(new ComplexNumber(1d, 1d));
        String expResult = "[(1.0 + 1.0j), (2.0 + 0.0j), (1.0 + 0.0j)]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStackVec method, of class ComplexNumberStack.
     */
    @Test
    public void testGetStackVec() {
        System.out.println("stringVec");
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1d, 0d));
        instance.push(new ComplexNumber(2d, 0d));
        instance.push(new ComplexNumber(1d, 1d));
        ComplexNumber[] expResult = {new ComplexNumber(1d, 1d), 
            new ComplexNumber(2d, 0d), new ComplexNumber(1d, 0d)};
        ComplexNumber[] result = instance.getStackVec();
        assertArrayEquals(expResult, result);
    } 
}
